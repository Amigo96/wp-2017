export class Video {
  public id: number;
  public title: string;
  public description: string;
  public url: string;
  public tags: string[];
  public category: string;
  public course: string;
}
