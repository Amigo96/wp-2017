import {Component, Input, OnInit} from '@angular/core';
import {Video} from '../../model/Video';
import {VideoService} from '../services/video/video.service';
import {ActivatedRoute, ParamMap} from '@angular/router';

@Component({
  selector: 'app-video-edit',
  templateUrl: './video-edit.component.html',
  styleUrls: ['./video-edit.component.css']
})
export class VideoEditComponent implements OnInit {

  public CREATE_ACTION = 'Create';
  public EDIT_ACTION = 'Edit';

  private _editingVideo: Video;


  private action = this.CREATE_ACTION;
  private isEdit = false;
  private newTag: string;

  protected video: Video;

  @Input('editingVideo')
  set setEditingVideo(editingVideo: Video) {

    console.log('Set editing video');
    this.setVideo(editingVideo);
  }

  private setVideo(editingVideo: Video) {
    console.log('Setting video');
    // if the editingVideo is not set, we should prevent the error
    if (editingVideo) {
      this.action = this.EDIT_ACTION;
      this.isEdit = true;
      this._editingVideo = editingVideo;
      this.video.title = editingVideo.title;
      this.video.description = editingVideo.description;
      this.video.tags = editingVideo.tags;
      this.video.course = editingVideo.course;
      this.video.url = editingVideo.url;
    }
  }



  constructor(private route: ActivatedRoute,
              private  videoService: VideoService) {
    this.video = new Video();
  }

  ngOnInit() {

    this.route.paramMap
      .switchMap((params: ParamMap) => {
        const videoTitle = params.get('title');

        console.info('params', params);
        console.info('title', videoTitle);
        const videoPromise = this.videoService
          .findByTitle(videoTitle);
        videoPromise.catch(
          error => {
            console.error(error.errorMessage);
          }
        );
        return videoPromise;
      })
      .subscribe(video => {
        console.info('video', video);
        this.setVideo(video);
      });
  }

  public save(): void {
    console.log('saving...');
    this.videoService.save(this.video)
      .then(videoFromServer => this.setVideo(videoFromServer));
    // we should reset the video instance after saving it
    this.video = new Video();
  }

  public edit() {
    this.videoService.edit(this._editingVideo, this.video)
      .then(videoFromServer => this.setVideo(videoFromServer));
  }

  public removeTag(tag: string) {
    const index = this.video.tags.indexOf(tag);
    console.log('removing tag: ', tag, ' at index ' + index);
    this.video.tags.splice(index, 1);
  }

  public addTag(tagName: string) {
    console.log('adding tag: ' + tagName);
    if (!this.video.tags) {
      this.video.tags = [];
    }
    this.video.tags.push(tagName);
    this.newTag = null;

  }

}
