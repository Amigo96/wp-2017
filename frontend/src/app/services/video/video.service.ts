import {Injectable} from '@angular/core';
import {Video} from '../../../model/Video';

@Injectable()
export class VideoService {

  constructor() {
  }

  private idSequence = 0;

  private videos = [{
    title: 'Angular 4 Components',
    description: `This video is from my Free Angular 4 Course: https://goo.gl/T5fqeB
Written tutorial: https://goo.gl/Ljd08U`,
    url: 'https://www.youtube.com/embed/giKeNoAWpOA',
    category: 'development',
    tags: ['angular', 'typescript', 'html'],
    course: 'Web Programming'
  }, {
    title: 'Templating Basics in Angular 4',
    description: `This video is from my Free Angular 4 Course: https://goo.gl/T5fqeB
Written tutorial: https://goo.gl/C9aN0U`,
    url: 'https://www.youtube.com/embed/m5k3z94rixA',
    category: 'development',
    tags: ['angular', 'typescript', 'html'],
    course: 'Web Programming'
  }, {
    title: 'Angular 4 Property Binding',
    description: `This video is from my Free Angular 4 Course: https://goo.gl/T5fqeB
Written tutorial: https://goo.gl/Z4MHTC`,
    url: 'https://www.youtube.com/embed/PKfKW5RW6k0',
    category: 'development',
    tags: ['angular', 'typescript', 'html'],
    course: 'Web Programming'
  }, {
    title: 'Angular 4 Services Tutorial',
    description: `This video is from my Free Angular 4 Course: https://goo.gl/T5fqeB
Written tutorial: https://goo.gl/96CN2Z`,
    url: 'https://www.youtube.com/embed/daxeYiv5FHk',
    category: 'development',
    tags: ['angular', 'typescript', 'html'],
    course: 'Web Programming'
  }];

  public latestVideos(): Promise<Video[]> {
    return Promise.resolve(this.videos);
  }

  save(video: Video): Promise<Video> {
    // simulation of the change that the async call will make
    const videosFromServer = [];
    Object.assign(videosFromServer, this.videos);
    this.videos = videosFromServer;

    this.idSequence++;
    video.id = this.idSequence;
    this.videos.push(video);

    return Promise.resolve(video);
  }

  edit(originalVideo: Video, updatedVideo: Video): Promise<Video> {
    // simulation of the change that the async call will make
    const videosFromServer = [];
    Object.assign(videosFromServer, this.videos);
    this.videos = videosFromServer;

    // copies all properties form updatedVideo into originalVideo
    // Object.assign(originalVideo, updatedVideo);

    return Promise.resolve(updatedVideo);
  }

  findByTitle(videoTitle: string): Promise<Video> {


    const result = this.videos.filter(video => video.title === videoTitle);
    if (result && result.length > 0) {
      return Promise.resolve(result[0]);
    } else {
      return Promise.reject({
        errorMessage: 'No video with the given title found',
        errorCode: 404
      });
    }
  }
}
